#include "contiki.h"
#include "lib/random.h"
#include "net/rime/rime.h"
#include "net/rime/collect.h"
#include "dev/leds.h"
#include "dev/button-sensor.h"
#include <stdlib.h>
#include "net/netstack.h"
#include <stdio.h>

static struct collect_conn tc;

/*---------------------------------------------------------------------------*/
PROCESS(example_collect_process, "Test collect process");
AUTOSTART_PROCESSES(&example_collect_process);

/*---------------------------------------------------------------------------*/
static void recv(const linkaddr_t *originator, uint8_t seqno, uint8_t hops) {}

/*---------------------------------------------------------------------------*/
static const struct collect_callbacks callbacks = { recv };
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(example_collect_process, ev, data)
{
  static struct etimer periodic;
  static struct etimer et;

  PROCESS_BEGIN();
  collect_open(&tc, 130, COLLECT_ROUTER, &callbacks);

  if ((linkaddr_node_addr.u8[0] == 2 &&
     linkaddr_node_addr.u8[1] == 0)) {
        printf("I am a sink\n");
	collect_set_sink(&tc, 2);
  }

  /* Allow some time for the network to settle. */
  etimer_set(&et, 120 * CLOCK_SECOND);
  PROCESS_WAIT_UNTIL(etimer_expired(&et));
  
  unsigned static int sensors[6];
  unsigned static int iDelay = 0;
  static const int ACCELERATE = 12;
  static const int STEADY = 15;
  static const int BREAK = 20;   

  sensors[0]=60; //initial value
  
  while(1) {
    /* Send a packet every 3 seconds. */
    if(etimer_expired(&periodic)) {
      etimer_set(&periodic, CLOCK_SECOND * 3);
      etimer_set(&et, random_rand() % (CLOCK_SECOND * 3));
    }

    PROCESS_WAIT_EVENT();

 
    if (iDelay < ACCELERATE) {  //accelerate
        sensors[0] = sensors[0] + abs(rand() % 15);
        iDelay++;
    }
    else if (iDelay >= ACCELERATE && iDelay < STEADY){   //keep speed more or less constant
        sensors[0]=sensors[0]+abs(rand()%2);
        iDelay++;
    }
    else if (iDelay >= STEADY && iDelay < BREAK){ //decrease some value to break
        sensors[0]=sensors[0] - abs(rand()%5);
    	iDelay++; 
    }
    else if (iDelay>= BREAK){ //start again
        iDelay = 0; 
        sensors[0] = 60;
    }
    sensors[2] = sensors[0]; //sensors[1] always reserved for datatype parameter
    sensors[3] = sensors[0];
    sensors[4] = sensors[0];
    sensors[1] = 86; //ASCII value for "V", indicate speed
    if (sensors[0] < 0 || sensors[0] > 150) // the sensor check the data collected is normal or not  
        sensors[5] = 1; //not normal
    else sensors[5] = 0; //normal value
    
    if(etimer_expired(&et) && !(linkaddr_node_addr.u8[0] == 2 &&
       linkaddr_node_addr.u8[1] == 0)) {
        printf("Sending\n");
        packetbuf_clear();
        packetbuf_copyfrom(sensors, sizeof(sensors));
    	collect_send(&tc, 15);
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
