#include "contiki.h"
#include "net/rime/rime.h"
#include <stdio.h>

/*---------------------------------------------------------------------------*/
PROCESS(example_unicast_process, "Example unicast");
AUTOSTART_PROCESSES(&example_unicast_process);

/*---------------------------------------------------------------------------*/
static const recv_uc(struct unicast_conn *c, const linkaddr_t *from) {
	printf("Alert received from the Coordinator that is generated from sensor node No. %d.%d \n ",
		*((unsigned int *)packetbuf_dataptr()), *((unsigned int *)packetbuf_dataptr()+1));
}
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
static const struct unicast_callbacks unicast_callbacks = {recv_uc};
static struct unicast_conn uc;
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(example_unicast_process, ev, data) {
	PROCESS_EXITHANDLER(unicast_close(&uc));
	PROCESS_BEGIN();
	unicast_open(&uc, 146, &unicast_callbacks);
	PROCESS_END();
}
/*---------------------------------------------------------------------------*/