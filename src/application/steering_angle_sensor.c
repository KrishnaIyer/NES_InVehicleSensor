#include "contiki.h"
#include "lib/random.h"
#include "net/rime/rime.h"
#include "net/rime/collect.h"
#include "dev/leds.h"
#include "dev/button-sensor.h"
#include <stdlib.h>
#include "net/netstack.h"
#include <stdio.h>

static struct collect_conn tc;

/*---------------------------------------------------------------------------*/
PROCESS(example_collect_process, "Test collect process");
AUTOSTART_PROCESSES(&example_collect_process);

/*---------------------------------------------------------------------------*/
static void recv(const linkaddr_t *originator, uint8_t seqno, uint8_t hops) { }

/*---------------------------------------------------------------------------*/
static const struct collect_callbacks callbacks = { recv };
short angle = 0;
char iDelay = 0;
static const char INIT_STRAIGHT_DELAY = 5;
static const char LEFT_DELAY = 12;
static const char STRAIGHT_DELAY = 17;
static const char RIGHT_DELAY = 25;
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(example_collect_process, ev, data)
{
  static struct etimer periodic;
  static struct etimer et;

  PROCESS_BEGIN();
  collect_open(&tc, 130, COLLECT_ROUTER, &callbacks);

  if((linkaddr_node_addr.u8[0] == 2 && linkaddr_node_addr.u8[1] == 0)) {
    printf("I am a sink\n");
    collect_set_sink(&tc, 2);
  }

  /* Allow some time for the network to settle. */
  etimer_set(&et, 120 * CLOCK_SECOND);
  PROCESS_WAIT_UNTIL(etimer_expired(&et));
  
  while(1) {

    /* Send a packet every 1 seconds. */
    if(etimer_expired(&periodic)) {
      etimer_set(&periodic, CLOCK_SECOND * 1); 
      etimer_set(&et, random_rand() % (CLOCK_SECOND * 1));
    }

    PROCESS_WAIT_EVENT();

    unsigned static int sensors[3]; 
    
    if (iDelay == RIGHT_DELAY) {
        angle = 0;
        sensors[0] = angle; // steering angle - max 270?
        iDelay = 0;
    }
    else if (iDelay < INIT_STRAIGHT_DELAY){
        iDelay++; 
        angle = 0;
        sensors[0] = angle;
    }
    else if (iDelay < LEFT_DELAY && iDelay >= INIT_STRAIGHT_DELAY) {
        iDelay++; 
        sensors[0] = angle - abs(rand() % 55);
    }
    else if (iDelay >= LEFT_DELAY && iDelay < STRAIGHT_DELAY) {
        angle = 0;
        iDelay++; 
        sensors[0] = angle;
    }
    else {
        iDelay++; 
        sensors[0] = angle + abs(rand() % 55);
    }

    sensors[1] = 65; // ASCII value for "A", indicate steering Angle
 
    if (angle > 270 || angle < -270) {
	sensors[2] = 1;
        angle = 0;
    }
    else sensors[2] = 0;

    if(etimer_expired(&et) && !(linkaddr_node_addr.u8[0] == 2 &&
       linkaddr_node_addr.u8[1] == 0)) {
        printf("Sending\n");
        packetbuf_clear();
        packetbuf_copyfrom(sensors, sizeof(sensors));
        collect_send(&tc, 15);
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/