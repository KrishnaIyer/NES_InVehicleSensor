#include "contiki.h"
#include "lib/random.h"
#include "net/rime/rime.h"
#include "net/rime/collect.h"
#include "dev/leds.h"
#include "dev/button-sensor.h"
#include <stdlib.h>
#include "net/netstack.h"
#include <stdio.h>

static struct collect_conn tc;

/*---------------------------------------------------------------------------*/
PROCESS(example_collect_process, "Test collect process");
AUTOSTART_PROCESSES(&example_collect_process);

/*---------------------------------------------------------------------------*/
static void recv(const linkaddr_t *originator, uint8_t seqno, uint8_t hops) { }

/*---------------------------------------------------------------------------*/
static const struct collect_callbacks callbacks = { recv };

/*---------------------------------------------------------------------------*/

PROCESS_THREAD(example_collect_process, ev, data)
{
  static struct etimer periodic;
  static struct etimer et;

  PROCESS_BEGIN();
  collect_open(&tc, 130, COLLECT_ROUTER, &callbacks);

  if((linkaddr_node_addr.u8[0] == 2 &&
      linkaddr_node_addr.u8[1] == 0)) {
    printf("I am a sink\n");
    collect_set_sink(&tc, 2);
  }

  /* Allow some time for the network to settle. */
  etimer_set(&et, 120 * CLOCK_SECOND);
  PROCESS_WAIT_UNTIL(etimer_expired(&et));
  
  while(1) {

    /* Send a packet every 1 seconds. */
    if(etimer_expired(&periodic)) {
      etimer_set(&periodic, CLOCK_SECOND * 1); 
      etimer_set(&et, random_rand() % (CLOCK_SECOND * 1));
    }

    PROCESS_WAIT_EVENT();
    
    double distance = 0; //initialize the distance value, indicating it only senses obstacles outside the car
    unsigned static int sensors[4]; //to change to sensors[3] if no any other data transmitted
    
    sensors[0] = distance + rand()%150; // rand%100 to get better simulation results, so it won't generate ridiculous large value, nor value within [0,100] too often
    sensors[1] = 68; // ASCII value for "D", indicate distance 
 // only send abnormal data
    if (sensors[0]>=0 && sensors[0] <= 100){//we will only measure till 100m
    	sensors[2] = 1; // sending alert, obstacle too close
       if(etimer_expired(&et) && !(linkaddr_node_addr.u8[0] == 2 &&
       linkaddr_node_addr.u8[1] == 0)) {
        printf("Sending\n");
        packetbuf_clear();
        packetbuf_copyfrom(sensors, sizeof(sensors));
        collect_send(&tc, 15);
       }
    }       
    else
    	sensors[2] = 0;




  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/