#include "contiki.h"
#include "lib/random.h"
#include "net/rime/rime.h"
#include "net/rime/collect.h"
#include "dev/leds.h"
#include "dev/button-sensor.h"
#include <stdlib.h>
#include "net/netstack.h"
#include <stdio.h>

static struct collect_conn tc;
static const struct unicast_callbacks unicast_callbacks = {};
static struct unicast_conn uc;

/*---------------------------------------------------------------------------*/
PROCESS(example_collect_process, "Test collect process");
AUTOSTART_PROCESSES(&example_collect_process);

/*---------------------------------------------------------------------------*/
static void recv(const linkaddr_t *originator, uint8_t seqno, uint8_t hops) {
    int a[2];
    a[0] = (originator->u8[0]);
    a[1] = originator->u8[1];
    linkaddr_t addr;
    addr.u8[0] = 1;
    addr.u8[1] = 0; 
    
    //first check which type of data is received
    int datatype = *((unsigned int *)packetbuf_dataptr()+1); // type 1 for speed, type 2 for distance, type 3 for steering angle
    int alert;
    int vehicledata[8];
    int i=0; // counter for printing the wheel speed
    
    //if it is steer angle or distance
    if (datatype == 65 || datatype == 68){
    	vehicledata[0] = *(unsigned int *)packetbuf_dataptr();
        alert = *((unsigned int *)packetbuf_dataptr()+2); // Each sensor node check if the data collected is normal or not and set the flag accordingly  
        printf("Model has received a message from Node No. %d.%d, hops %d, type %c data: %d\n",originator->u8[0], originator->u8[1], hops, datatype, vehicledata[0]);
    }
    
    
    else { //the data is speed data 
      
        vehicledata[0]=1; //wheel speed No.1
        vehicledata[1]= *(unsigned int *)packetbuf_dataptr();
        
        vehicledata[2]=2; //wheel speed No.2
        vehicledata[3]= *((unsigned int *)packetbuf_dataptr()+2);
        
        vehicledata[4]=3; //wheel speed No.3
        vehicledata[5]= *((unsigned int *)packetbuf_dataptr()+3);
        
        vehicledata[6]=4; //wheel speed No.4
        vehicledata[7]= *((unsigned int *)packetbuf_dataptr()+4);
        
        alert = *((unsigned int *)packetbuf_dataptr()+5); // Each sensor node check if the data collected is normal or not and set the flag accordingly      	
        printf("Model has received a message from Node No. %d.%d, hops %d, type %c data: ",originator->u8[0], originator->u8[1], hops, datatype);
        for (i=0; i<8; i++)
        	printf("%d ",vehicledata[i]);
        printf("\n");
    }
   
    

    if (alert == 1) { // The coordinator generates alert  
        packetbuf_copyfrom(a, sizeof(a));
        unicast_send(&uc, &addr);
        printf("Sending\n"); //showing the coordinator is sending signals 
    }        
}

/*---------------------------------------------------------------------------*/
static const struct collect_callbacks callbacks = { recv };
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(example_collect_process, ev, data) {
    static struct etimer periodic;
    static struct etimer et;

    PROCESS_BEGIN();
    collect_open(&tc, 130, COLLECT_ROUTER, &callbacks);
    unicast_open(&uc, 146, &unicast_callbacks);

    if((linkaddr_node_addr.u8[0] == 2 && linkaddr_node_addr.u8[1] == 0)) {
        printf("I am a sink\n");
	collect_set_sink(&tc, 2);
    }

    PROCESS_WAIT_EVENT();

    PROCESS_END();
}
/*---------------------------------------------------------------------------*/
