In Vehicle WSN for Raw Sensor Data
===================================
This is our study project on implementing a WSN for collecting Raw sensor data from a Vehicle.

You can watch the complete presentation of our project on YouTube:  https://youtu.be/IxZojF_ta7E


Simulation
==========
Here we look at simulation using the Contiki OS and Cooja Simulator.


Set-ubuntu 
-----------
Note: This setup is made for Ubuntu 17.04 using Gnome.

* Install QEMU 						: $ sudo apt-get install QEMU
* Unpack the .ova file 				: $ tar xvf *.ova
* List available QEMU formats 		: $ qemu-img -h | tail -n1
* Convert to a supported format 	: $ qemu-img convert -O qcow2 *.vmdk Contiki.qcow2
* Load the virtual machine			: $ qemu-system-x86_64 --enable-kvm -m 1024 Contiki.qcow2
	* Note: The -m is used for RAM Allocation
* Setting up eclipse in the VM:
	* $ cd bin
	* wget http://ftp.halifax.rwth-aachen.de/eclipse//technology/epp/downloads/release/neon/R/eclipse-cpp-neon-R-linux-gtk-x86_64.tar.gz
	* tar xf eclipse-cpp-neon-R-linux-gtk-x86_64.tar.gz
	* sudo apt-get install g++

	
	
	
Interference
------------
Create a simulation of type MRM.

Run
---
To run the simulation, simply open the two saved simulation files under /src in Cooja and start simulation; 

Or, you can follow the steps to build your own simulation:
1. turn on the virtual machine
2. copy the application and interference folders  under /src to /home/computation/contiki/examples/  
3. open a new simulation in Cooja 
4. add the Sky motes from /home/computation/contiki/examples/application
5. start the simulation to see the results 
6. add interference as shown in the screen cast video: https://youtu.be/IxZojF_ta7E
7. reload and start the simulation to see results with interference.


Serial Output
-------------
The following steps can be used to output the value of any mote on a serial port
* Select tools->serial socket(SERVER) -> Your Mote
* Enter the serial port value (usually 8080)
* Open Telnet in a terminal window outside Cooja: $telnet localhost 8080
* Start the simulation.
* You should now see the output of Your Mote in Cooja piped into the serial port and available on the terminal window.

References
==========
- https://www.qemu.org/download/
- https://wiki.hackzine.org/sysadmin/kvm-import-ova.html
- https://en.wikibooks.org/wiki/QEMU/Images
- https://askubuntu.com/questions/858649/how-can-i-copypaste-from-the-host-to-a-kvm-guest