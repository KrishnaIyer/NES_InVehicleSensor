Backend
=======
In this module, the data that is sent on the serial part of the gateway is received and processed for backend modelling.
- This whole module is written in Python since it's easier.


Graphs in Python
================
To generate graphs in Python, we can use the plotly library:
 - https://plot.ly/python/getting-started/#initialization-for-offline-plotting

The following steps should help you get started
 - Install pip(if not already found) : $ sudo apt-get install python pip
 - Install the plotly libraries: $ sudo pip install plotly
 - Run in offline mode: Check the examples under examples/.

 
Serial Communication
====================
Python has excellent serial Communication libraries. 
- https://docs.python.org/2/howto/sockets.html
- https://docs.python.org/3/library/socket.html
 
